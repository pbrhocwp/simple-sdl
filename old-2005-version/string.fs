\
\ String utility
\
: append ( a1 n2 a2 --)
    over over >r >r count chars + swap chars move
    r> r> dup >r c@ + r> c! ;

create tabstring 1000 chars allot

: i" ( --)    \ Begining of a string
    postpone s" postpone tabstring postpone place ; immediate

: a" ( --)    \ Append on string
    postpone s" postpone tabstring postpone append ; immediate

: append$ ( a n --)
    tabstring append ;

: string$ ( -- a n)
    tabstring count ;

: .string$ ( --)
    string$ type ;


: test-string ( --)
    i" Hello " a" this " a" is a "
    s" test." append$
    string$ swap . .  .string$
    i"  Done!" .string$
;