include simple-sdl.fs
include string.fs


\ TODO:
\   Associer une touche a un son




: db ( --)  CR .S CR ;



variable grid   500 grid !

: SEC ( n --)
    1000 * MS ;

: init ( --)
    z" Forth Synth" 800 600 0 10000 sdl-init ;

: close ( --)
    sdl-close ;

: _ ( --)
    grid @ MS ." _" cr ;

: __ ( --) _ _ ;
: ___ ( --) _ _ _ ;
: ____ ( --) _ _ _ _ ;

: tempo ( n --)
    grid ! ;



\ Keys part
create tabkeys 200 cells allot
variable keys#   0 keys# !

: a-key ( n -- addr)
    tabkeys swap 2 cells * + ;
: xt-sound ( n -- addr)
    tabkeys swap 2 cells * cell + + ;

: add-key ( key nsound --)
   keys# @ a-key !      ' keys# @ xt-sound !
    1 keys# +! ;


: add-sound ( a1 u1  a2 u2 --) \ 1:to print 2:file to play
    ftoc sdl-load-sound
    create , , ,
  does> dup @ sdl-play-sound
    dup 2 cells + @   swap 1 cells + @ type space ;


: play-key-sound ( --)
    keys# @ 0 do
	I a-key @ sdl-key-press+reset if
	    I xt-sound @ execute
	then loop ;



variable now-tempo  0 now-tempo !
: display-tempo ( --)
    sdl-get-ticks dup now-tempo @ -  grid @  > if
	." _ " cr  now-tempo ! else drop then ;




: key-loop ( --)
    0  \ Leave the end condition on the stack
    begin
	grid @ sdl-poll-event
	play-key-sound
	display-tempo
	SDLK_ESCAPE sdl-key-press if drop 1 then
	dup
    until
    drop
;


\
\ Recording part
\
: set-mixer ( --)
    s" amixer set 'Capture' cap" system
    s" amixer set 'Capture' 5%" system
;

: start-rec ( --)
    s" arecord -t wav -f cd  -c 2 output.wav &" system ;

: stop-rec ( --)
    s" pkill arecord" system ;

: play-rec ( --)
    s" aplay output.wav &" system ;

: aud ( --)
    s" audacity output.wav &" system ;



\
\ Pitch part
\
: ->gen ( a1 n1 a2 n2 -- a n)  \ 1:pitch number 2:filename
    2swap i" soundstretch "    append$
    a"  output-pitch.wav -pitch="    append$
    string$ system   s" output-pitch.wav"
;



: ->re ( a n -- a n) s" 2" ->gen ;
: ->mi ( a n -- a n) s" 4" ->gen ;
: ->fa ( a n -- a n) s" 5" ->gen ;
: ->sol ( a n -- a n) s" 7" ->gen ;
: ->la ( a n -- a n) s" 9" ->gen ;
: ->si ( a n -- a n) s" 11" ->gen ;
: ->do ( a n -- a n) s" 12" ->gen ;


: <-si ( a n -- a n) s" -2" ->gen ;
: <-la ( a n -- a n) s" -4" ->gen ;
: <-sol ( a n -- a n) s" -5" ->gen ;
: <-fa ( a n -- a n) s" -7" ->gen ;
: <-mi ( a n -- a n) s" -9" ->gen ;
: <-re ( a n -- a n) s" -11" ->gen ;
: <-do ( a n -- a n) s" -12" ->gen ;
