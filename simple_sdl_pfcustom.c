/* @(#) pfcustom.c 20/04/23 1.3 */

#ifdef PF_USER_CUSTOM

/***************************************************************
** Call Custom Functions for pForth
**
** Create a file similar to this and compile it into pForth
** by setting -DPF_USER_CUSTOM="mycustom.c"
**
** Using this, you could, for example, call X11 from Forth.
** See "pf_cglue.c" for more information.
**
** Author: Phil Burk
** Custom: Philippe Brochard
** Copyright 1994 3DO, Phil Burk, Larry Polansky, David Rosenboom
**           Philippe Brochard
**
** The pForth software code is dedicated to the public domain,
** and any third party may reproduce, distribute and modify
** the pForth software code or any derivative works thereof
** without any compensation or license.  The pForth software
** code is provided on an "as is" basis without any warranty
** of any kind, including, without limitation, the implied
** warranties of merchantability and fitness for a particular
** purpose and their equivalents under the laws of any jurisdiction.
**
***************************************************************/


#include "pf_all.h"
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>

/* Please, edit this for your project */
#define MAX_IMAGE 1000
#define MAX_KEY 300
#define MAX_SOUND 300

int sdl_done = 0;
/* int sdl_mouse_button_1 = 0; */
/* int sdl_mouse_button_2 = 0; */
/* int sdl_mouse_button_3 = 0; */
/* int sdl_mouse_button_4 = 0; */
/* int sdl_mouse_button_5 = 0; */
int sdl_mouse_x = 0;
int sdl_mouse_y = 0;

int tab_mouse_button[3];
int tab_keys[MAX_KEY];

int image_count = -1;
SDL_Texture * tab_images[MAX_IMAGE];

int sound_count = -1;
Mix_Chunk * tab_sounds[MAX_SOUND];

SDL_Window * main_screen;
SDL_Renderer *main_renderer;
static TTF_Font * font = NULL;

/* server part */
int server_fd;
struct sockaddr_in address;
int opt = 1;
int addrlen = sizeof(address);
char server_pad[1024] = {0};

static void sdl_init (cell_t width, cell_t height, cell_t fullscreen);
static void sdl_close (void);
static void sdl_delay (cell_t ms);
static void sdl_show_cursor (void);
static void sdl_hide_cursor (void);
static void set_tab_keys_from_keysym(int keysym, int value);
static void analyze_event (SDL_Event * event);
static void sdl_poll_event (void);
static cell_t sdl_get_done (void);
static void sdl_reset_done (void);
static void sdl_present (void);
static cell_t sdl_key_press (cell_t key);
static cell_t sdl_mouse_button (cell_t button);
static cell_t sdl_get_mouse_x (void);
static cell_t sdl_get_mouse_y (void);
static void sdl_background (cell_t red, cell_t green, cell_t blue);
static cell_t sdl_load_image (cell_t filename_p);
static void sdl_put_image (cell_t image, cell_t x, cell_t y);
static cell_t sdl_open_font (cell_t filename_p, cell_t ptsize);
static void sdl_put_text (cell_t string_p, cell_t x, cell_t y, cell_t forecol);
static cell_t sdl_text_width (cell_t string);
static cell_t sdl_text_height (cell_t string);
static void sdl_play_sound(cell_t snd);
static void sdl_halt_sound(void);
static void sdl_set_sound_volume(cell_t percent);
static cell_t sdl_load_sound (cell_t filename_p);
/* server part */
static cell_t server_start (cell_t port);
static cell_t server_accept (void);
static cell_t server_read(cell_t socket);
static cell_t server_send (cell_t msg, cell_t socket);
static cell_t server_send_line (cell_t msg, cell_t socket);

/****************************************************************
** Step 1: Put your own special glue routines here
**     or link them in from another file or library.
****************************************************************/

/*===========================================================
 *		Open and close functions
 ============================================================*/
void sdl_init (cell_t width, cell_t height, cell_t fullscreen) {
	Uint32 videoflags = 0;
	int i;
	const char *title = "TODO: Move this into a word";

	sdl_done = 0;

	printf("INIT: width=%d, height=%d\n", width, height);

	if ( IMG_Init(IMG_INIT_PNG) < 0 ) {
		fprintf(stderr, "Couldn't initialize SDL Image: %s\n", SDL_GetError());
		exit(1);
	}
	atexit(IMG_Quit);

	if ( SDL_Init(SDL_INIT_EVERYTHING) < 0 ) {
		fprintf(stderr, "Couldn't initialize SDL: %s\n", SDL_GetError());
		exit(1);
	}
	atexit(SDL_Quit);

	if ( TTF_Init() < 0 ) {
		fprintf(stderr, "Couldn't initialize TTF: %s\n", SDL_GetError());
		exit(2);
	}
	atexit(TTF_Quit);

	if (fullscreen == 1) {
		videoflags ^= SDL_WINDOW_FULLSCREEN_DESKTOP;
	}

	if ((main_screen = SDL_CreateWindow("Atlast SDL",
										 SDL_WINDOWPOS_UNDEFINED,
										 SDL_WINDOWPOS_UNDEFINED,
										 width, height,
										 videoflags)) == NULL) {
		fprintf(stderr, "Couldn't set %dx%d video mode: %s\n", width, height, SDL_GetError());
		exit(2);
	}

	SDL_SetWindowTitle (main_screen, title);
	SDL_SetWindowInputFocus(main_screen);

	if ((main_renderer = SDL_CreateRenderer(main_screen, -1, 0)) == NULL) {
		fprintf(stderr, "Couldn't create renderer: %s\n", SDL_GetError());
		exit(2);
	}

	/* Sound initialisation */
	if (SDL_Init(SDL_INIT_AUDIO) < 0)
		{
			fprintf(stderr,
					"\nWarning: I could not initialize audio!\n"
					"The Simple DirectMedia error that occured was:\n"
					"%s\n\n", SDL_GetError());
		}

	/* if (Mix_OpenAudio(22050, AUDIO_S16, 2, 512) < 0) */
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096) < 0)
		{
			fprintf(stderr,
					"\nWarning: I could not set up audio for 44100 Hz "
					"16-bit stereo.\n"
					"The Simple DirectMedia error that occured was:\n"
					"%s\n\n", SDL_GetError());
		}
	Mix_Volume(-1, 20 * MIX_MAX_VOLUME / 100);

	/* allocate mixing channels */
	Mix_AllocateChannels(100);

	for (i = 0; i < MAX_KEY; i++) {
		tab_keys[i] = 0;
	}

	for (i = 0; i < 3; i++) {
		tab_mouse_button[i] = 0;
	}
}

void sdl_close (void) {
	SDL_DestroyRenderer(main_renderer);
	SDL_DestroyWindow(main_screen);

	if ( !(font == NULL)) {
		TTF_CloseFont(font);
		font = NULL;
	}

	Mix_CloseAudio();

	IMG_Quit();
	SDL_Quit();
}

void sdl_delay (cell_t ms) {
	SDL_Delay(ms);
}

void sdl_show_cursor (void) {
	SDL_ShowCursor (SDL_ENABLE);
}

void sdl_hide_cursor (void) {
	SDL_ShowCursor (SDL_DISABLE);
}


void set_tab_keys_from_keysym(int keysym, int value) {
	switch(keysym) {
	case SDLK_UP:            tab_keys[27] = value; break;
	case SDLK_DOWN:          tab_keys[28] = value; break;
	case SDLK_LEFT:          tab_keys[29] = value; break;
	case SDLK_RIGHT:         tab_keys[30] = value; break;
	case SDLK_RETURN:        tab_keys[31] = value; break;
	case SDLK_SPACE:         tab_keys[32] = value; break;
	case SDLK_TAB:           tab_keys[33] = value; break;
	case SDLK_LCTRL:         tab_keys[34] = value; break;
	case SDLK_KP_DIVIDE:     tab_keys[35] = value; break;
	case SDLK_KP_MULTIPLY:   tab_keys[36] = value; break;
	case SDLK_KP_MINUS:      tab_keys[37] = value; break;
	case SDLK_KP_PLUS:       tab_keys[38] = value; break;
	case SDLK_KP_ENTER:      tab_keys[39] = value; break;
	case SDLK_KP_1:          tab_keys[40] = value; break;
	case SDLK_KP_2:          tab_keys[41] = value; break;
	case SDLK_KP_3:          tab_keys[42] = value; break;
	case SDLK_KP_4:          tab_keys[43] = value; break;
	case SDLK_KP_5:          tab_keys[44] = value; break;
	case SDLK_KP_6:          tab_keys[45] = value; break;
	case SDLK_KP_7:          tab_keys[46] = value; break;
	case SDLK_KP_8:          tab_keys[47] = value; break;
	case SDLK_KP_9:          tab_keys[48] = value; break;
	case SDLK_KP_0:          tab_keys[49] = value; break;
	case SDLK_KP_PERIOD:     tab_keys[50] = value; break;
	}
}

void analyze_event (SDL_Event * event) {
	switch( event->type ) {
	case SDL_QUIT : sdl_done = 1; break;

	case SDL_MOUSEBUTTONDOWN :
		if (event->button.button == SDL_BUTTON_LEFT) tab_mouse_button[0] = 1;
		if (event->button.button == SDL_BUTTON_MIDDLE) tab_mouse_button[1] = 1;
		if (event->button.button == SDL_BUTTON_RIGHT) tab_mouse_button[2] = 1;
		break;

	case SDL_MOUSEBUTTONUP :
		if (event->button.button == SDL_BUTTON_LEFT) tab_mouse_button[0] = 0;
		if (event->button.button == SDL_BUTTON_MIDDLE) tab_mouse_button[1] = 0;
		if (event->button.button == SDL_BUTTON_RIGHT) tab_mouse_button[2] = 0;
		break;

	case SDL_KEYDOWN :
		printf ("In C- Key press: %s %d 0x%02X\n", SDL_GetKeyName(event->key.keysym.sym), event->key.keysym.sym, event->key.keysym.scancode);
		if (event->key.keysym.sym == SDLK_ESCAPE) {
			sdl_done = -1;
		}

		if (event->key.keysym.sym >= SDLK_a && event->key.keysym.sym <= SDLK_z) {
			tab_keys[event->key.keysym.sym - SDLK_a] = 1;
		} else {
			set_tab_keys_from_keysym(event->key.keysym.sym, 1);
		}
		break;
	case SDL_KEYUP :
		printf ("In C- Key release: %s %d 0x%02X\n", SDL_GetKeyName(event->key.keysym.sym), event->key.keysym.sym, event->key.keysym.scancode);
		if (event->key.keysym.sym >= SDLK_a && event->key.keysym.sym <= SDLK_z) {
			tab_keys[event->key.keysym.sym - SDLK_a] = 0;
		} else {
			set_tab_keys_from_keysym(event->key.keysym.sym, 0);
		}
		break;
	default:
		break;
	}
}

void sdl_poll_event (void) {
	SDL_Event event;

	while(SDL_PollEvent(&event)) {
		analyze_event(&event);
	}

	SDL_GetMouseState (&sdl_mouse_x, &sdl_mouse_y);
}

cell_t sdl_get_done (void) {
	return sdl_done;
}

void sdl_reset_done () {
  sdl_done = 0;
}

void sdl_present (void) {
	SDL_RenderPresent(main_renderer);
}

cell_t sdl_key_press (cell_t key) {
	return tab_keys[key];
}

cell_t sdl_mouse_button (cell_t button) {
	return tab_mouse_button[button - 1];
}

cell_t sdl_get_mouse_x (void) {
	return sdl_mouse_x;
}

cell_t sdl_get_mouse_y (void) {
	return sdl_mouse_y;
}

void sdl_background (cell_t red, cell_t green, cell_t blue) {
	SDL_SetRenderDrawColor(main_renderer, red, green, blue, 255);
	SDL_RenderClear(main_renderer);
}

cell_t sdl_load_image (cell_t filename_p) {
	SDL_Surface * surf = NULL;
	char filename[1024];

	ForthStringToC( filename, (char *) filename_p, 1023);

	image_count += 1;
	if (image_count >= MAX_IMAGE) {
		image_count -= 1;
		fprintf (stderr, "Sorry can't load more than %d images\n", MAX_IMAGE);
		return 0;
	}

	printf ("In C - Opening: %s as image %d\n", filename, image_count);

	surf = IMG_Load (filename);
	if (surf == NULL) {
		image_count -= 1;
		fprintf(stderr, "Can't load image %s - %s\n", filename, SDL_GetError());
		return 0;
	}

	tab_images[image_count] = SDL_CreateTextureFromSurface(main_renderer, surf);
	if (tab_images[image_count] == NULL) {
		image_count -= 1;
		fprintf(stderr, "Can't load image %s - %s\n", filename, SDL_GetError());
		return 0;
	}
	SDL_FreeSurface(surf);

	return image_count;
}

void sdl_put_image (cell_t image, cell_t x, cell_t y) {
	SDL_Rect dstrect;
	int w, h;

	SDL_QueryTexture(tab_images[image], NULL, NULL, &w, &h);
	dstrect.x = x; dstrect.y = y;
	dstrect.w = w; dstrect.h = h;
	SDL_RenderCopy(main_renderer, tab_images[image], NULL, &dstrect);
}

/* ,---------- */
/* | Font part */
/* `---------- */
cell_t sdl_open_font (cell_t filename_p, cell_t ptsize) {
	char filename[1024];

	ForthStringToC( filename, (char *) filename_p, 1023);

	if ( !(font == NULL)) {
		TTF_CloseFont(font);
	}

	font = TTF_OpenFont(filename, ptsize);
	if ( font == NULL ) {
		fprintf(stderr, "Couldn't load %d pt font from %s: %s\n",
				ptsize, filename, SDL_GetError());
		return 0;
	}
	TTF_SetFontStyle(font, TTF_STYLE_NORMAL);

	return 0;
}

void sdl_put_text (cell_t string_p, cell_t x, cell_t y, cell_t forecol) {
	SDL_Color fg;
	SDL_Surface *text;
	SDL_Rect dstrect;
	SDL_Texture *texture;
	char string[1024];

	ForthStringToC( string, (char *) string_p, 1023);

	fg.r = (forecol >> 16) & 0xff;
	fg.g = (forecol >> 8) & 0xff;
	fg.b = forecol & 0xff;

	text = TTF_RenderText_Solid(font, string, fg);

	if ( text != NULL ) {
		dstrect.x = x;
		dstrect.y = y;
		dstrect.w = text->w;
		dstrect.h = text->h;
		texture = SDL_CreateTextureFromSurface(main_renderer, text);
		SDL_RenderCopy(main_renderer, texture, NULL, &dstrect);
		SDL_FreeSurface(text);
	}

	return;
}

cell_t sdl_text_width (cell_t string_p) {
	SDL_Surface *text;
	SDL_Color fg;
	cell_t ret = 0;
	char string[1024];

	ForthStringToC( string, (char *) string_p, 1023);

	text = TTF_RenderText_Solid(font, string, fg);

	if ( text != NULL ) {
		ret = text->w;
		SDL_FreeSurface(text);
	}

	return ret;
}


cell_t sdl_text_height (cell_t string_p) {
	SDL_Surface *text;
	SDL_Color fg;
	cell_t ret = 0;
	char string[1024];

	ForthStringToC( string, (char *) string_p, 1023);

	text = TTF_RenderText_Solid(font, string, fg);

	if ( text != NULL ) {
		ret = text->h;
		SDL_FreeSurface(text);
	}

	return ret;
}

/* ,----------- */
/* | Sound part */
/* `----------- */
void sdl_play_sound(cell_t snd) {
	Mix_PlayChannel(-1, tab_sounds[snd], 0);
}

void sdl_halt_sound(void) {
	Mix_HaltChannel(-1);
}

void sdl_set_sound_volume(cell_t percent) {
	Mix_Volume(-1, percent * MIX_MAX_VOLUME / 100);
}


cell_t sdl_load_sound (cell_t filename_p) {
	char filename[1024];

	ForthStringToC( filename, (char *) filename_p, 1023);

	sound_count += 1;
	if (sound_count >= MAX_SOUND)
		{
			fprintf (stderr, "Sorry can't load more than %d sounds\n", MAX_SOUND);
			sound_count -= 1;
			return 0;
		}

	tab_sounds[sound_count] = Mix_LoadWAV(filename);
	if (tab_sounds[sound_count] == NULL) {
		fprintf(stderr,
				"\nError: I could not load the sound file:\n"
				"%s\n"
				"The Simple DirectMedia error that occured was:\n"
				"%s\n\n", filename, SDL_GetError());
		sound_count -= 1;
		return 0;
	}
	return sound_count;
}

/* ,------------- */
/* | Network part */
/* `------------- */
cell_t server_start (cell_t port) {
	/* Creating socket file descriptor */
	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
		perror("socket failed");
		return 0;
	}

	/* Forcefully attaching socket to the port `port` */
	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
				   &opt, sizeof(opt))) {
		perror("setsockopt");
		return 0;
	}
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons( port );

	/* Forcefully attaching socket to the port `port` */
	if (bind(server_fd, (struct sockaddr *)&address,
			 sizeof(address))<0) {
		perror("bind failed");
		return 0;
	}

	if (listen(server_fd, 3) < 0) {
		perror("listen");
		return 0;
	}

	printf("Server started on port %d\n", port);

	return -1;
}

cell_t server_accept (void) {
	cell_t new_socket;
	if ((new_socket = accept(server_fd, (struct sockaddr *)&address,
							 (socklen_t*)&addrlen))<0) {
		perror("accept");
		return 0;
	}
	return new_socket;
}

cell_t server_read(cell_t socket) {
	char buffer[1024] = {0};

	if (read( socket , buffer, 1024) < 0) {
		perror("read failed");
		return 0;
	}

	CStringToForth(server_pad, buffer, 1024);

	return (cell_t) server_pad;
}

cell_t server_send (cell_t msg, cell_t socket) {
	char buffer[1024] = {0};

	ForthStringToC( buffer, (char *) msg, 1023);

	if (send(socket, buffer, strlen(buffer), 0) < 0) {
		perror("send failed");
		return 0;
	}
	return -1;
}

cell_t server_send_line (cell_t msg, cell_t socket) {
	char buffer[1024] = {0};

	ForthStringToC( buffer, (char *) msg, 1023);

	buffer[strlen(buffer)] = '\n';

	if (send(socket, buffer, strlen(buffer), 0) < 0) {
		perror("send_line failed");
		return 0;
	}
	return -1;
}

/****************************************************************
** Step 2: Create CustomFunctionTable.
**     Do not change the name of CustomFunctionTable!
**     It is used by the pForth kernel.
****************************************************************/

#ifdef PF_NO_GLOBAL_INIT
/******************
** If your loader does not support global initialization, then you
** must define PF_NO_GLOBAL_INIT and provide a function to fill
** the table. Some embedded system loaders require this!
** Do not change the name of LoadCustomFunctionTable()!
** It is called by the pForth kernel.
*/
#define NUM_CUSTOM_FUNCTIONS  (2)
CFunc0 CustomFunctionTable[NUM_CUSTOM_FUNCTIONS];

Err LoadCustomFunctionTable( void )
{
	CustomFunctionTable[0] = sdl_init;
	CustomFunctionTable[1] = sdl_wait;
	CustomFunctionTable[2] = sdl_show_cursor;
	CustomFunctionTable[3] = sdl_hide_cursor;
	CustomFunctionTable[4] = sdl_poll_event;
	CustomFunctionTable[5] = sdl_get_done;
	CustomFunctionTable[6] = sdl_reset_done;
	CustomFunctionTable[7] = sdl_present;
	CustomFunctionTable[8] = sdl_key_press;
	CustomFunctionTable[9] = sdl_mouse_button;
	CustomFunctionTable[10] = sdl_get_mouse_x;
	CustomFunctionTable[11] = sdl_get_mouse_y;
	CustomFunctionTable[12] = sdl_background;
	CustomFunctionTable[13] = sdl_load_image;
	CustomFunctionTable[14] = sdl_put_image;
	CustomFunctionTable[15] sdl_open_font;
	CustomFunctionTable[16] sdl_put_text;
	CustomFunctionTable[17] sdl_text_width;
	CustomFunctionTable[18] sdl_text_height;
	CustomFunctionTable[19] sdl_play_sound;
	CustomFunctionTable[20] sdl_halt_sound;
	CustomFunctionTable[21] sdl_set_sound_volume;
	CustomFunctionTable[22] sdl_load_sound;
	/* server part */
	CustomFunctionTable[23] server_start;
	CustomFunctionTable[24] server_accept;
	CustomFunctionTable[25] server_read;
	CustomFunctionTable[26] server_send;
	CustomFunctionTable[27] server_send_line;

	CustomFunctionTable[28] = sdl_close;
	return 0;
}

#else
/******************
** If your loader supports global initialization (most do.) then just
** create the table like this.
*/
CFunc0 CustomFunctionTable[] =
{
 (CFunc0) sdl_init,
 (CFunc0) sdl_delay,
 (CFunc0) sdl_show_cursor,
 (CFunc0) sdl_hide_cursor,
 (CFunc0) sdl_poll_event,
 (CFunc0) sdl_get_done,
 (CFunc0) sdl_reset_done,
 (CFunc0) sdl_present,
 (CFunc0) sdl_key_press,
 (CFunc0) sdl_mouse_button,
 (CFunc0) sdl_get_mouse_x,
 (CFunc0) sdl_get_mouse_y,
 (CFunc0) sdl_background,
 (CFunc0) sdl_load_image,
 (CFunc0) sdl_put_image,
 (CFunc0) sdl_open_font,
 (CFunc0) sdl_put_text,
 (CFunc0) sdl_text_width,
 (CFunc0) sdl_text_height,
 (CFunc0) sdl_play_sound,
 (CFunc0) sdl_halt_sound,
 (CFunc0) sdl_set_sound_volume,
 (CFunc0) sdl_load_sound,
 /* server-part */
 (CFunc0) server_start,
 (CFunc0) server_accept,
 (CFunc0) server_read,
 (CFunc0) server_send,
 (CFunc0) server_send_line,

 (CFunc0) sdl_close
};
#endif

/****************************************************************
** Step 3: Add custom functions to the dictionary.
**     Do not change the name of CompileCustomFunctions!
**     It is called by the pForth kernel.
****************************************************************/

#if (!defined(PF_NO_INIT)) && (!defined(PF_NO_SHELL))
Err CompileCustomFunctions( void )
{
	Err err;
	int i = 0;
/* Compile Forth words that call your custom functions.
** Make sure order of functions matches that in LoadCustomFunctionTable().
** Parameters are: Name in UPPER CASE, Function, Index, Mode, NumParams
*/
	err = CreateGlueToC( "SDL-INIT", i++, C_RETURNS_VOID, 3 );           if( err < 0 ) return err;
	err = CreateGlueToC( "SDL-DELAY", i++, C_RETURNS_VOID, 1 );          if( err < 0 ) return err;
	err = CreateGlueToC( "SDL-SHOW-CURSOR", i++, C_RETURNS_VOID, 0 );    if( err < 0 ) return err;
	err = CreateGlueToC( "SDL-HIDE-CURSOR", i++, C_RETURNS_VOID, 0 );    if( err < 0 ) return err;
	err = CreateGlueToC( "SDL-POLL-EVENT", i++, C_RETURNS_VOID, 0 );     if( err < 0 ) return err;
	err = CreateGlueToC( "SDL-DONE?", i++, C_RETURNS_VALUE, 0 );         if( err < 0 ) return err;
	err = CreateGlueToC( "SDL-RESET-DONE", i++, C_RETURNS_VOID, 0 );     if( err < 0 ) return err;
	err = CreateGlueToC( "SDL-PRESENT", i++, C_RETURNS_VOID, 0 );        if( err < 0 ) return err;
	err = CreateGlueToC( "SDL-KEY-PRESS", i++, C_RETURNS_VALUE, 1 );     if( err < 0 ) return err;
	err = CreateGlueToC( "SDL-MOUSE-BUTTON", i++, C_RETURNS_VALUE, 1 );  if( err < 0 ) return err;
	err = CreateGlueToC( "SDL-MOUSE-X", i++, C_RETURNS_VALUE, 0 );       if( err < 0 ) return err;
	err = CreateGlueToC( "SDL-MOUSE-Y", i++, C_RETURNS_VALUE, 0 );       if( err < 0 ) return err;
	err = CreateGlueToC( "SDL-BACKGROUND", i++, C_RETURNS_VOID, 3 );     if( err < 0 ) return err;
	err = CreateGlueToC( "SDL-LOAD-IMAGE", i++, C_RETURNS_VALUE, 1 );    if( err < 0 ) return err;
	err = CreateGlueToC( "SDL-PUT-IMAGE", i++, C_RETURNS_VOID, 3 );      if( err < 0 ) return err;
	err = CreateGlueToC( "SDL-OPEN-FONT", i++, C_RETURNS_VALUE, 2 );     if( err < 0 ) return err;
	err = CreateGlueToC( "SDL-PUT-TEXT", i++, C_RETURNS_VOID, 4 );       if( err < 0 ) return err;
	err = CreateGlueToC( "SDL-TEXT-WIDTH", i++, C_RETURNS_VALUE, 1 );    if( err < 0 ) return err;
	err = CreateGlueToC( "SDL-TEXT-HEIGHT", i++, C_RETURNS_VALUE, 1 );   if( err < 0 ) return err;
	err = CreateGlueToC( "SDL-PLAY-SOUND", i++, C_RETURNS_VOID, 1 );     if( err < 0 ) return err;
	err = CreateGlueToC( "SDL-HALT-SOUND", i++, C_RETURNS_VOID, 0 );     if( err < 0 ) return err;
	err = CreateGlueToC( "SDL-VOLUME", i++, C_RETURNS_VOID, 1 );         if( err < 0 ) return err;
	err = CreateGlueToC( "SDL-LOAD-SOUND", i++, C_RETURNS_VALUE, 1 );    if( err < 0 ) return err;
	/* server part */
	err = CreateGlueToC( "SERVER-START", i++, C_RETURNS_VALUE, 1 );      if( err < 0 ) return err;
	err = CreateGlueToC( "SERVER-ACCEPT", i++, C_RETURNS_VALUE, 0 );     if( err < 0 ) return err;
	err = CreateGlueToC( "SERVER-READ", i++, C_RETURNS_VALUE, 1 );       if( err < 0 ) return err;
	err = CreateGlueToC( "SERVER-SEND", i++, C_RETURNS_VALUE, 2 );       if( err < 0 ) return err;
	err = CreateGlueToC( "SERVER-SEND-LINE", i++, C_RETURNS_VALUE, 2 );  if( err < 0 ) return err;

	err = CreateGlueToC( "SDL-CLOSE", i++, C_RETURNS_VOID, 0 );          if( err < 0 ) return err;

	return 0;
}
#else
Err CompileCustomFunctions( void ) { return 0; }
#endif

/****************************************************************
** Step 4: Recompile using compiler option PF_USER_CUSTOM
**         and link with your code.
**         Then rebuild the Forth using "pforth -i system.fth"
**         Test:   10 Ctest0 ( should print message then '11' )
****************************************************************/

#endif  /* PF_USER_CUSTOM */
