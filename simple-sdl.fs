include lib.fs
include random.fs

\ We need to make some C strings, so we include the forth to c
\ routine from above:
: ftoc ( c-addr u -- 'c-addr )
	here dup >r over allot swap cmove 0 c, align r> ;

: z" ( -- )  postpone s" postpone ftoc ; immediate

library libsdl ./libsimple-sdl.so

libsdl init int int int (void) sdl_init
libsdl close (void) sdl_close

libsdl delay int (void) sdl_delay
libsdl show-cursor (void) sdl_show_cursor
libsdl hide-cursor (void) sdl_hide_cursor

libsdl testfun int (int) testfun

libsdl poll-event (void) sdl_poll_event

libsdl present (void) sdl_present

libsdl pressed? int (int) sdl_key_press

libsdl mouse-button int (int) sdl_mouse_button
libsdl mouse-x (int) sdl_get_mouse_x
libsdl mouse-y (int) sdl_get_mouse_y

: mouse@ ( -- x y) mouse-x mouse-y ;

libsdl background int int int (void) sdl_background

libsdl load-image ptr (int) sdl_load_image
libsdl put-image int int int (void) sdl_put_image

libsdl open-font ptr int (int) sdl_open_font
libsdl put-text ptr int int int (void) sdl_put_text
libsdl text-width ptr (int) sdl_text_width
libsdl text-height ptr (int) sdl_text_height

libsdl load-sound ptr (int) sdl_load_sound
libsdl play-sound int (void) sdl_play_sound
libsdl halt-sound (void) sdl_halt_sound
libsdl set-sound-volume int (void) sdl_set_sound_volume

libsdl get-ticks (int) sdl_get_ticks
libsdl begin-game (void) sdl_begin_game
libsdl done? (int) sdl_is_done

: :image  ( addr n --)
  ftoc load-image create ,
  does> @ ;

: :sound    ( addr n --)
  ftoc load-sound create ,
  does> @ ;

0  constant SDLK_a
1  constant SDLK_b
2  constant SDLK_c
3  constant SDLK_d
4  constant SDLK_e
5  constant SDLK_f
6  constant SDLK_g
7  constant SDLK_h
8  constant SDLK_i
9  constant SDLK_j
10 constant SDLK_k
11 constant SDLK_l
12 constant SDLK_m
13 constant SDLK_n
14 constant SDLK_o
15 constant SDLK_p
16 constant SDLK_q
17 constant SDLK_r
18 constant SDLK_s
19 constant SDLK_t
20 constant SDLK_u
21 constant SDLK_v
22 constant SDLK_w
23 constant SDLK_x
24 constant SDLK_y
25 constant SDLK_z

27 constant SDLK_UP
28 constant SDLK_DOWN
29 constant SDLK_LEFT
30 constant SDLK_RIGHT
31 constant SDLK_RETURN
32 constant SDLK_SPACE
33 constant SDLK_TAB
34 constant SDLK_LCTRL
35 constant SDLK_KP_DIVIDE
36 constant SDLK_KP_MULTIPLY
37 constant SDLK_KP_MINUS
38 constant SDLK_KP_PLUS
39 constant SDLK_KP_ENTER
40 constant SDLK_KP_1
41 constant SDLK_KP_2
42 constant SDLK_KP_3
43 constant SDLK_KP_4
44 constant SDLK_KP_5
45 constant SDLK_KP_6
46 constant SDLK_KP_7
47 constant SDLK_KP_8
48 constant SDLK_KP_9
49 constant SDLK_KP_0
50 constant SDLK_KP_PERIOD
