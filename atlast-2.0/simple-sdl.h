#ifndef __SIMPLE_SDL_H
#define __SIMPLE_SDL_H

#include <unistd.h>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>

extern struct primfcn simplesdlp[];

#endif
