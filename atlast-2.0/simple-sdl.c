#include <stdio.h>
#include <stdlib.h>
#include "atldef.h"
#include <simple-sdl.h>

///* Please, edit this for your project */
#define MAX_IMAGE 1000
#define MAX_KEY 27
//#define MAX_SOUND 1000
//
//int test_var = 10;
//
int sdl_done = 0;
///* int sdl_mouse_button_1 = 0; */
///* int sdl_mouse_button_2 = 0; */
///* int sdl_mouse_button_3 = 0; */
///* int sdl_mouse_button_4 = 0; */
///* int sdl_mouse_button_5 = 0; */
int sdl_mouse_x = 0;
int sdl_mouse_y = 0;

int tab_mouse_button[3];
int tab_keys[MAX_KEY];

int image_count = -1;
SDL_Texture * tab_images[MAX_IMAGE];

//int sound_count = -1;
//Mix_Chunk * tab_sounds[MAX_SOUND];
//
//
//int sdl_escape_key = SDLK_ESCAPE;
//int sdl_up_key = SDLK_UP;
//int sdl_down_key = SDLK_DOWN;
//int sdl_left_key = SDLK_LEFT;
//int sdl_right_key = SDLK_RIGHT;

SDL_Window * main_screen;
SDL_Renderer *main_renderer;
SDL_Texture *main_texture;
//static SDL_Surface * background;
static TTF_Font * font = NULL;
//
//void putpixel (SDL_Surface *surface, int x, int y, Uint32 pixel);
//void set_background_color (SDL_Surface *surface, Uint32 color);
//prim sdl_show_background ();

// /*===========================================================
//  *		Open and close functions
//  ============================================================*/

prim sdl_init () {
	int width, height, fullscreen;
	Uint32 videoflags = 0;
	int i;

	Sl(3);

	width = S2;
	height = S1;
	fullscreen = S0;

	sdl_done = 0;

	char *title = "TODO: Move this into a word";

	if ( IMG_Init(IMG_INIT_PNG) < 0 ) {
		fprintf(stderr, "Couldn't initialize SDL Image: %s\n", SDL_GetError());
		exit(1);
	}
	atexit(IMG_Quit);

	if ( SDL_Init(SDL_INIT_EVERYTHING) < 0 ) {
		fprintf(stderr, "Couldn't initialize SDL: %s\n", SDL_GetError());
		exit(1);
	}
	atexit(SDL_Quit);

	if ( TTF_Init() < 0 ) {
		fprintf(stderr, "Couldn't initialize TTF: %s\n", SDL_GetError());
		exit(2);
	}
	atexit(TTF_Quit);

	if (fullscreen == 1) {
		videoflags ^= SDL_WINDOW_FULLSCREEN_DESKTOP;
	}

	if ((main_screen = SDL_CreateWindow("Atlast SDL",
										 SDL_WINDOWPOS_UNDEFINED,
										 SDL_WINDOWPOS_UNDEFINED,
										 width, height,
										 videoflags)) == NULL) {
		fprintf(stderr, "Couldn't set %dx%d video mode: %s\n", width, height, SDL_GetError());
		exit(2);
	}

	SDL_SetWindowTitle (main_screen, title);
	SDL_SetWindowInputFocus(main_screen);

	if ((main_renderer = SDL_CreateRenderer(main_screen, -1, 0)) == NULL) {
		fprintf(stderr, "Couldn't create renderer: %s\n", SDL_GetError());
		exit(2);
	}

	if ((main_texture = SDL_CreateTexture(main_renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, width, height)) == NULL) {
		fprintf(stderr, "Couldn't create texture: %s\n", SDL_GetError());
		exit(2);
	}

	/* Sound initialisation */
	if (SDL_Init(SDL_INIT_AUDIO) < 0)
		{
			fprintf(stderr,
					"\nWarning: I could not initialize audio!\n"
					"The Simple DirectMedia error that occured was:\n"
					"%s\n\n", SDL_GetError());
		}

	/* if (Mix_OpenAudio(22050, AUDIO_S16, 2, 512) < 0) */
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096) < 0)
		{
			fprintf(stderr,
					"\nWarning: I could not set up audio for 44100 Hz "
					"16-bit stereo.\n"
					"The Simple DirectMedia error that occured was:\n"
					"%s\n\n", SDL_GetError());
		}
	Mix_Volume(-1, 66 * MIX_MAX_VOLUME / 100);

	// allocate mixing channels
	Mix_AllocateChannels(100);

	for (i = 0; i < MAX_KEY; i++) {
		tab_keys[i] = 0;
	}

	for (i = 0; i < 3; i++) {
		tab_mouse_button[i] = 0;
	}

	Npop(3);
}

prim sdl_close () {
	SDL_DestroyTexture(main_texture);
	SDL_DestroyRenderer(main_renderer);
	SDL_DestroyWindow(main_screen);

	if ( !(font == NULL)) {
		TTF_CloseFont(font);
		font = NULL;
	}

	Mix_CloseAudio();

	IMG_Quit();
	SDL_Quit();
}

prim sdl_delay () {
	Sl(1);

	SDL_Delay(S0);

	Pop;
}

prim sdl_show_cursor () {
	SDL_ShowCursor (SDL_ENABLE);
}

prim sdl_hide_cursor () {
	SDL_ShowCursor (SDL_DISABLE);
}


void analyze_event (SDL_Event * event) {
	switch( event->type ) {
	case SDL_QUIT : sdl_done = 1; break;

	case SDL_MOUSEBUTTONDOWN :
		if (event->button.button == SDL_BUTTON_LEFT) tab_mouse_button[0] = 1;
		if (event->button.button == SDL_BUTTON_MIDDLE) tab_mouse_button[1] = 1;
		if (event->button.button == SDL_BUTTON_RIGHT) tab_mouse_button[2] = 1;
		break;

	case SDL_MOUSEBUTTONUP :
		if (event->button.button == SDL_BUTTON_LEFT) tab_mouse_button[0] = 0;
		if (event->button.button == SDL_BUTTON_MIDDLE) tab_mouse_button[1] = 0;
		if (event->button.button == SDL_BUTTON_RIGHT) tab_mouse_button[2] = 0;
		break;

	case SDL_KEYDOWN :
		//	tab_keys[event->key.keysym.sym] = 1;
		printf ("In C- Key press: %s %d 0x%02X\n", SDL_GetKeyName(event->key.keysym.sym), event->key.keysym.sym, event->key.keysym.scancode);
		//sdl_done = (event->key.keysym.sym == 113);
		if (event->key.keysym.sym == SDLK_q || event->key.keysym.sym == SDLK_UP || event->key.keysym.sym == SDLK_ESCAPE) {
			sdl_done = 1;
		}

		if (event->key.keysym.sym >= SDLK_a && event->key.keysym.sym <= SDLK_z) {
			printf("PPPPPPPPPPPPPPPPPPPPPPPP\n");
			tab_keys[event->key.keysym.sym - SDLK_a] = 1;
		}
		break;
	case SDL_KEYUP :
		printf ("In C- Key release: %s %d 0x%02X\n", SDL_GetKeyName(event->key.keysym.sym), event->key.keysym.sym, event->key.keysym.scancode);
		//	tab_keys[event->key.keysym.sym] = 0;
		if (event->key.keysym.sym >= SDLK_a && event->key.keysym.sym <= SDLK_z) {
			printf("Release PPPPPPPPPPPPPPPPPPPPPPPP\n");
			tab_keys[event->key.keysym.sym - SDLK_a] = 0;
		}
		break;
	default:
		break;
	}
}

prim sdl_poll_event () {
	//SDL_Event event;
	//
	//while(SDL_PollEvent(&event)) {
	//	analyze_event(&event);
	//}
	SDL_Event ev;
	if (SDL_PollEvent(&ev) == 0) {
		//printf(".\n");
		SDL_Delay(1);
		return;
	}

	switch(ev.type) {
	case SDL_QUIT:
		printf("Received SDL_QUIT - bye!\n");
		return;

	case SDL_MOUSEBUTTONUP:
		printf("SDL_MOUSEBUTTONUP, button %d clicks %d\n", ev.button.button, (int)ev.button.clicks);
		break;

	case SDL_MOUSEBUTTONDOWN:
		printf("SDL_MOUSEBUTTONDOWN, button %d clicks %d\n", ev.button.button, (int)ev.button.clicks);
		break;

	case SDL_KEYUP:
	case SDL_KEYDOWN:
		if(ev.type == SDL_KEYUP)
			printf("SDL_KEYUP: ");
		else
			printf("SDL_KEYDOWN: ");

		printf("Keycode: %s (%d) Scancode: %s (%d)\n",
			   SDL_GetKeyName(ev.key.keysym.sym), ev.key.keysym.sym,
			   SDL_GetScancodeName(ev.key.keysym.scancode),
			   ev.key.keysym.scancode);

		if(ev.key.keysym.sym == SDLK_q) {
			printf("You pressed Q - bye!\n");
			sdl_done = 1;
			return;
		}

		if(ev.key.keysym.sym == SDLK_UP) {
			printf("You pressed UP PRESSED !!!!!!\n");
		}

		break;

	case SDL_MOUSEWHEEL:
		printf("MouseWheel: x: %d, y: %d\n", ev.wheel.x, ev.wheel.y);
		break;

	case SDL_TEXTINPUT:
		printf("SDL_TEXTINPUT: %s\n", ev.text.text ? ev.text.text : "<NULL>");
		break;

	//case SDL_JOYBUTTONDOWN:
	//	printf("SDL_JOYBUTTONDOWN dev %d button %d state %d\n",
	//		   (int)ev.jbutton.which, (int)ev.jbutton.button, (int)ev.jbutton.state);
	//	break;
	//case SDL_JOYBUTTONUP:
	//	printf("SDL_JOYBUTTONUP dev %d button %d state %d\n",
	//		   (int)ev.jbutton.which, (int)ev.jbutton.button, (int)ev.jbutton.state);
	//	break;

	case SDL_MOUSEMOTION:
	case SDL_FINGERDOWN:
	case SDL_FINGERUP:
	case SDL_FINGERMOTION:
		break; // ignore

	case SDL_WINDOWEVENT:
		break;

	default:
		// printf("SDL_Event of type: 0x%x received\n", ev.type);
		break;
	}
}

prim sdl_present () {
	SDL_RenderPresent(main_renderer);
}


// int
// sdl_key_press (int key)
// {
//	return tab_keys[key];
// }
//
// int
// sdl_key_press_reset (int key)
// {
//	int val = tab_keys[key];
//
//	tab_keys[key] = 0;
//
//	return val;
// }
//
//
//
// int
// sdl_mouse_button (int button)
// {
//	return tab_mouse_button[button - 1];
// }
//
// int
// sdl_get_mouse_x (void)
// {
//	return sdl_mouse_x;
// }
//
//
// int
// sdl_get_mouse_y (void)
// {
//	return sdl_mouse_y;
// }

prim sdl_background () {
	Sl(3);
	SDL_SetRenderDrawColor(main_renderer, S2, S1, S0, 255);
	SDL_RenderClear(main_renderer);
	Npop(3);
}

prim sdl_load_image () {
	SDL_Surface * surf = NULL;

	Sl(1);
	So(1);
	Hpc(S0);

	image_count += 1;
	if (image_count >= MAX_IMAGE) {
		image_count -= 1;
		fprintf (stderr, "Sorry can't load more than %d images\n", MAX_IMAGE);
		S0 = -1;
		return;
	}

	/*printf ("In C - Try to open: %s as image %d\n", (char *) S0, image_count);*/

	surf = IMG_Load ((char *) S0);
	if (surf == NULL) {
		image_count -= 1;
		fprintf(stderr, "Can't load image %s - %s\n", (char *) S0, SDL_GetError());
		S0 = -1;
		return;
	}

	tab_images[image_count] = SDL_CreateTextureFromSurface(main_renderer, surf);
	SDL_FreeSurface(surf);

	S0 = image_count;
}

prim sdl_put_image () {
	SDL_Rect dstrect;
	int w, h;

	Sl(3);

	SDL_QueryTexture(tab_images[S2], NULL, NULL, &w, &h);
	dstrect.x = S1; dstrect.y = S0;
	dstrect.w = w; dstrect.h = h;
	SDL_RenderCopy(main_renderer, tab_images[S2], NULL, &dstrect);

	Npop(3);
}

// int
// sdl_open_font (char * filename, int ptsize)
// {
//	if ( !(font == NULL))
//		TTF_CloseFont(font);
//
//	font = TTF_OpenFont(filename, ptsize);
//	if ( font == NULL ) {
//		fprintf(stderr, "Couldn't load %d pt font from %s: %s\n",
//				ptsize, filename, SDL_GetError());
//		return -1;
//	}
//	TTF_SetFontStyle(font, TTF_STYLE_NORMAL);
//
//	return 0;
// }
//
// void
// sdl_put_text (char * string, int x, int y, int forecol)
// {
//	SDL_Color fg;
//	/* SDL_Color bg; */
//	SDL_Surface *text;
//	SDL_Rect dstrect;
//
//	fg.r = (forecol >> 16) & 0xff;
//	fg.g = (forecol >> 8) & 0xff;
//	fg.b = forecol & 0xff;
//
//	/* text = TTF_RenderText_Shaded(font, string, fg, bg); */
//	text = TTF_RenderText_Solid(font, string, fg);
//
//	if ( text != NULL ) {
//		dstrect.x = x;
//		dstrect.y = y;
//		dstrect.w = text->w;
//		dstrect.h = text->h;
//		SDL_BlitSurface(text, NULL, main_screen, &dstrect);
//		SDL_FreeSurface(text);
//	}
//
//	return;
// }
//
// int
// sdl_text_width (char *string)
// {
//	SDL_Surface *text;
//	SDL_Color fg;
//	int ret = 0;
//
//	text = TTF_RenderText_Solid(font, string, fg);
//
//	if ( text != NULL ) {
//		ret = text->w;
//		SDL_FreeSurface(text);
//	}
//
//	return ret;
// }
//
//
// int
// sdl_text_height (char *string)
// {
//	SDL_Surface *text;
//	SDL_Color fg;
//	int ret = 0;
//
//	text = TTF_RenderText_Solid(font, string, fg);
//
//	if ( text != NULL ) {
//		ret = text->h;
//		SDL_FreeSurface(text);
//	}
//
//	return ret;
// }
//
//
// void
// sdl_put_line (int x1, int y1, int x2, int y2, int color)
// {
//	int i, d;
//	float dx, dy;
//
//	Uint32 c = SDL_MapRGB(main_screen->format,
//						  (color >> 16) & 0xff,
//						  (color >> 8) & 0xff,
//						  color & 0xff);
//
//	/* Lock the screen for direct access to the pixels */
//	if ( SDL_MUSTLOCK(main_screen) )
//		{
//			if ( SDL_LockSurface(main_screen) < 0 )
//				{
//					fprintf(stderr, "Can't lock screen: %s\n", SDL_GetError());
//					return;
//				}
//		}
//
//	if (x1 < 0) x1 = 0;
//	if (x1 >= main_screen->w) x1 = main_screen->w - 1;
//	if (x2 < 0) x2 = 0;
//	if (x2 >= main_screen->w) x2 = main_screen->w - 1;
//
//	if (y1 < 0) y1 = 0;
//	if (y1 >= main_screen->h) y1 = main_screen->h - 1;
//	if (y2 < 0) y2 = 0;
//	if (y2 >= main_screen->h) y2 = main_screen->h - 1;
//
//	/* Hocwp: This is the slow method: use a more fast algorithm */
//	if (abs (x2 - x1) < abs (y2 - y1)) {
//		d = abs (y2 - y1);
//	} else {
//		d = abs (x2 - x1);
//	}
//
//	dx = (x2 - x1) / (float) d;
//	dy = (y2 - y1) / (float) d;
//
//	for (i = 0; i < d; i++)
//		putpixel (main_screen, x1 + dx * i, y1 + dy * i, c);
//
//	if ( SDL_MUSTLOCK(main_screen) )
//		{
//			SDL_UnlockSurface(main_screen);
//		}
//
//	/* Update just the part of the display that we've changed */
//	/*  Hocwp: all the screen is redrawed in the main loop... */
//	/* SDL_UpdateRect(main_screen, x1, y1, 1, 1); */
//	/*   SDL_UpdateRect(main_screen, x2, y2, 1, 1); */
//
//	return;
// }
//
//
//
//
// /* Sound part */
// prim sdl_play_sound() {
//	Sl(1);
//
//	Mix_PlayChannel(-1, tab_sounds[S0], 0);
//	Pop;
// }
//
// prim sdl_halt_sound() {
//	Mix_HaltChannel(-1);
// }
//
// prim sdl_set_sound_volume() {
//	Sl(1);
//
//	Mix_Volume(-1, S0 * MIX_MAX_VOLUME / 3);
//	Pop;
// }
//
//
// prim sdl_load_sound () {
//	Sl(1);
//	So(1);
//	Hpc(S0);
//
//	sound_count += 1;
//	if (sound_count >= MAX_SOUND)
//		{
//			fprintf (stderr, "Sorry can't load more than %d sounds\n", MAX_SOUND);
//			S0 = -1;
//			return;
//		}
//
//	tab_sounds[sound_count] = Mix_LoadWAV((char *) S0);
//
//	if (tab_sounds[sound_count] == NULL)
//		{
//			fprintf(stderr,
//					"\nError: I could not load the sound file:\n"
//					"%s\n"
//					"The Simple DirectMedia error that occured was:\n"
//					"%s\n\n", (char *) S0, SDL_GetError());
//		}
//	S0 = sound_count;
// }

prim sdl_get_ticks () {
	So(1);
	Push = SDL_GetTicks ();
}

prim sdl_is_done () {
	So(1);
	Push = (sdl_done != 0);
}

struct primfcn simplesdlp[] = {
	{"0INIT", sdl_init},
	{"0CLOSE", sdl_close},
	{"0DELAY", sdl_delay},
	{"0SHOW-CURSOR", sdl_show_cursor},
	{"0HIDE-CURSOR", sdl_hide_cursor},
	{"0POLL-EVENT", sdl_poll_event},
	{"0PRESENT", sdl_present},
	{"0BACKGROUND", sdl_background},
	{"0LOAD-IMAGE", sdl_load_image},
	{"0PUT-IMAGE", sdl_put_image},
//	{"0PLAY-SOUND", sdl_play_sound},
//	{"0HALT-SOUND", sdl_halt_sound},
//	{"0SET-SOUND-VOLUME", sdl_set_sound_volume},
//	{"0LOAD-SOUND", sdl_load_sound},
	{"0GET-TICKS", sdl_get_ticks},
	{"0DONE?", sdl_is_done},
	{NULL,      (codeptr) 0}
};
