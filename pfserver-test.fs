." Forth: starting server" cr
9999 server-start
if ." Forth: server started" cr then

." Forth: open socket" cr
server-accept constant sock
sock if ." Forth: listen on socket" cr then

." Forth: read from client" cr .s
sock server-read
dup if ." Forth: OK received string" cr then

." Forth: on stack" cr
.s

count .s
type

c" BEGIN:---" sock server-send
if ." Forth: Send OK" cr then

c" OK OK OK from FORTH!!!!!" sock server-send-line
if ." Forth: Send OK" cr then

c" >ONE MORE<" sock server-send-line
if ." Forth: Send OK" cr then

." Forth: after send" cr
.s

: $->pad ( c-addr n -- pad-addr ) dup pad ! pad 1+ swap cmove pad ;

: .st ( c-addr --) sock server-send-line drop ;
: .t ( n --) s>d <# #s #> $->pad .st ;

variable done? done? off

: repl
  begin
	c" OK " sock server-send drop
	sock server-read
	dup count 2 - s" bye" compare
	if count evaluate else done? on then
	done? @
  until
;

repl
