include simple-sdl.fs

1024 800 0 init

s" gnome-emacs.png" :image gnome
s" ball.png" :image ball
s" link.jpg" :image link
s" bomb.wav" :sound bomb

variable ball-x 100 ball-x !

: cls 120 130 30 background  present ;

: keyboard?
  SDLK_a pressed? if ." a pressed!" cr then
  SDLK_b pressed? if bomb play-sound then
  SDLK_UP pressed? if ." UPPUPUPUPU" cr then
  SDLK_RIGHT pressed? if 10 ball-x +! then
;

: draw
  20 130 30 background
  link 0 0 put-image
  gnome  100 100 put-image
  ball   ball-x @ 100 put-image
  z" Un test en Forth!!!" 200 200 $EE0033 put-text
  present
;

: main
  begin-game
  z" font/Vera.ttf" 30 open-font drop

  5 set-sound-volume
  bomb play-sound

  begin
    draw
    poll-event
    \ ." x=" mouse-x . ." y=" mouse-y . cr
    \ ." button 1=" 1 mouse-button .
    \ ." button 2=" 2 mouse-button .
    \ ." button 3=" 3 mouse-button . cr
    keyboard?
    
    10 delay
    done?
  until
;

main

close

bye
