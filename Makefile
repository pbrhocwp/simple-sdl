CC=gcc
PROG=simple-sdl
FLAGS=-Wall `sdl2-config --cflags --libs` -lSDL2_image -lSDL2_ttf -lSDL2_mixer -I. -fPIC

all: libsimple-sdl.so
	$(CC) $(FLAGS) -o lib$(PROG).so -shared $(PROG).c
	gcc $(FLAGS) -I.  -L. -lsimple-sdl -o test test.c
	g++ $(FLAGS) -I.  -L. -lsimple-sdl -o test++ test.cpp


libsimple-sdl.so: simple-sdl.c simple-sdl.h

standalone:
	$(CC) $(FLAGS) -o $(PROG) $(PROG).c

# pforth part
pforth: simple_sdl_pfcustom.c
	if ! test -d pforth-git; then git clone https://github.com/philburk/pforth.git pforth-git; fi
	cp simple_sdl_pfcustom.c pforth-git/csrc/pfcustom.c
	cd pforth-git/build/unix/ && make EXTRA_CCOPTS='-DPF_USER_CUSTOM="pfcustom.c" $(FLAGS)' LDADD='$(FLAGS)'
	cp pforth-git/build/unix/pforth* .

clean:
	rm -rf *~ *.so *.o test test++
	rm -rf pforth pforth_standalone pforth.dic
	cd pforth-git/build/unix/ && make clean

mrproper: clean
	rm -rf pforth-git
